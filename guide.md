# Explore Halifax

![Halifax waterfront](images/halifax.png)

This repository puts together a collection of recommendations to explore Halifax, NS, Canada by computer science students at Dalhousie.

## Historical places

### Bloomfield Centre
[**Bloomfield Centre**](https://www.google.com/maps/place/Bloomfield+Centre/@44.6577634,-63.5974437,15z/data=!4m5!3m4!1s0x0:0x73a24b22d3209c27!8m2!3d44.6578018!4d-63.597394?hl=en) If looking for a cool abandoned place to practice skateboarding the old school Bloomfield centre is a place to go. It is located by Robie st and Almon st.\
![Bloomfield Centre](images/bloomfield-school.jpeg) <br/>
<small> Note: This image has been taken from https://www.cbc.ca/news/canada/nova-scotia/bloomfield-site-school-sold-developer-1.5896245, on May 13th, 2022 </small>


**Citadel Hill**\
![Citadel Hill](images/citadel.jpg) <br/>
Note: This image has been taken from https://www.tripadvisor.com/Attractions-g154976-Activities-c47-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html, on May 5, 2022

There are a lot of Historical Places to visit in Halifax. The Citadel Hill tops the list. It is a hill which has a harbour underneath itself.
More information about it could be found at https://www.novascotia.com/see-do/attractions/halifax-citadel-national-historic-site/1440

Citadel Hill - Historical military base in Halifax. Link: https://www.pc.gc.ca/en/lhn-nhs/ns/halifax

[**Point Pleasant park**](https://www.novascotia.com/see-do/attractions/point-pleasant-park/1461) has a few military historical sites, and a nice view of the Atlantic ocean.

[**Citadel Hill**](https://www.pc.gc.ca/en/lhn-nhs/ns/halifax) large historical hill with a great view of Halifax.

[**Halifax Public Gardens**](https://www.google.com/travel/things-to-do?g2lb=2502548,2503771,2503781,4258168,4270442,4284970,4291517,4306835,4429192,4515404,4597339,4649665,4722900,4723331,4733969,4738606,4747696,4757164,4758493,4762561,4762570,4771572,4771758&hl=en-CA&gl=ca&ssta=1&dest_mid=/m/02qjb7z&dest_state_type=main&dest_src=ts&q=halifax+historical+places&sa=X&ved=2ahUKEwiX14OklMv3AhVCkIkEHQraAFoQuL0BegQIChAv) Friends of the Public Gardens offer free horticultural and historical tours of the gardens

[**York Redoubt**](https://www.pc.gc.ca/en/lhn-nhs/ns/york/index?utm_source=gmb&utm_medium=yorkredoubt) Old military defense base.

[**Canadian Forces Base**](https://www.cafconnection.ca/Halifax/In-My-Community/About-CFB-Halifax.aspx ) Canadian Forces Base (CFB) Halifax is one of the  Canadian largest military bases and home to Canada's East Coast Navy. It was found in 1906. Now Canadian Forces Base supports Maritime Forces Atlantic 
and allocated lodger units with administrative, logistics, information technology, port operations and emergency services.

[**Dingle Tower**](https://scotiasites.com/dingle-tower/) was created to commemorate the 150th anniversary of the establishment of the government in Nova Scotia. The tower was built from 1908-1912. From the top of the tower you can get a nice view of the Northwest Arm.

**Citadel Hill**\
At Citadel Hill visitors get the opportunity to explore the history of the fortress and the soldiers who were stationed there as well as touch a piece of Halifax's Military history\
link: https://www.halifaxcitadel.ca/

[**Halifax Seaport Farmers Market**](https://www.halifaxfarmersmarket.com/) Located on the Halifax waterfront, it has various local vendors selling a variety of goods

### Fairview Lawn Cemetery
[**Fairview Lawn Cemetery**](https://en.wikipedia.org/wiki/Fairview_Lawn_Cemetery) located at [**3720 Windsor St**](https://goo.gl/maps/EgGptiasJj1m8hgc6) Is a cemetery best known for being the final resting place of 121 victims from the sinking of the Titanic. The cemetery also contains 29 war graves of Commonwealth service personnel.
![Fairview Cemetery](images/fairviewCemetery.jpg) </br>
<small>Note: This image has been taken from https://en.wikipedia.org/wiki/Fairview_Lawn_Cemetery on May 12, 2022 </small>

### Dalhousie University

![Dalhousie University](images/Dal.PNG) </br>

<small> Note: This image has been taken from https://signalhfx.ca/dalhousie-university-confirms-jan-31-resumption-of-in-person-classes/ </small><br><br>
[**Dalhousie University**](https://www.tripadvisor.ca/Attraction_Review-g154976-d677952-Reviews-Dalhousie_University-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html) is a public research university that is more than 200 years old. Some of its historic infrastructures are worth visiting.
### Africville Park

[**Africville Park**](https://www.pc.gc.ca/apps/dfhd/page_nhs_eng.aspx?id=1763) used to be the location of an African Nova Scotian neighbourhood before it was demolished for the purpose of urban renewal. It now hosts a community park for all to enjoy, including a mountain biking circuit.



**Point Pleasant Park**
It is a really nice park at the South End (near the port). At this park, people can walk，ride and feel the sea breeze and nature, and there are several small fortresses hidden in the woods, this is a great place to relax in your free time.

[**The Halliburton**](https://thehalliburton.com/) is a historic boutique hotel known for its redbrick exterior. With unique artwork inside, as well as a charming restaurant, it is a lovely place to stay when visiting the city. Its location is within walking distance of the harbor and of downtown Halifax.

## Art gallery

### Art gallery of Nova Scotia

[**Art gallery of Nova Scotia**](https://www.artgalleryofnovascotia.ca/) With locations in downtown Halifax and Yarmouth, the Art Gallery of Nova Scotia is the largest art museum in Atlantic Canada.(from Art gallery websites)

### Studio 21
[**Studio 21**](https://studio21.ca/) is an art studio located at [**5431 Doyle St #102**](https://g.page/Studio21FineArt?share) featuring Canadian artists, with half of the artists being from the Atlantic region. The exhibitions change monthly and have shipping available worldwide.
![Studio 21](images/studio21.jpg) </br>
<small>Note: This image was taken from https://www.novascotia.com/see-do/fine-arts/studio-21-fine-art/2623 on May 12, 2022</small>


## Museums

**Thomas Mcculloch Museum**\
![Museum](images/whale.jpg) <br/>
Note: This image has been taken from https://mapsus.net/CA/thomas-mcculloch-museum-188625 on May 5, 2022
<br/>Though Halifax has a lot of Museums, however, one of the most accessible museum is within the Dalhousie University. It is
called Thomas Mcculloch Museum. This museum resides in the Department of Biology, in the Life Science Building at Dalhousie.
This museum has one of the biggest collections of birds, beetles, butterflies, moths, fishes, mushrooms species. One of the
most interesting facts about the museum is that it houses the vertebrate piece of Indian whale and the museum itself followed by Aquatron.
More information about the museum could be found at https://www.dal.ca/faculty/science/biology/research/facilities/thomas-mcculloch-museum.html

[**The Canadian museum of immigration**](https://pier21.ca/) is located next to the seaport farmers market. The museum details Halifax's long history of being a landing port for immigrants.

[**Discovery Centre**](https://thediscoverycentre.ca/) is fun and interactive science museum in Halifax.

**Canadian Museum of Immigration at Pier 21**\
Pier 21 is a National Historic site and it is site of Atlantic Canada's only national museum\
Link: https://pier21.ca/

[**Museum of Natural History**](https://naturalhistory.novascotia.ca/) a kind of science, it can help people know the natural history

### [Martime Museum of the Atlantic](https://maritimemuseum.novascotia.ca/)

Learn about the maritime history deeply interwoven into the heart of Halifax and Nova Scotia. Explore everything from how the Mi'kmaw peoples interacted with the ocean, the days of sailing, to WW1 and WW2 era Canadian naval history. Has the world's most important wooden Titanic artifacts.

### Beyond Van Gogh Halifax
[**Beyond Van Gogh Halifax**](https://vangoghhalifax.com/) is a art museum which is a new truly immersive experience. If you are artistic you will love this as the floors, ceilings and walls transform into art. The atmosphere causes you to feel like you have ventured directly into a Van Gogh painting.

## Parks

### Admiral Cove Park
[**Admiral Cove Park**](https://www.halifaxtrails.ca/admiral-cove-park/) it is a beautiful hidden spot in bedford Halifax with an amazing view and lots of trails in mountains for hiking
![Admiral Cove Park](images/park.jpeg) <br/>
<small> Note: This image has been taken from https://www.halifaxtrails.ca/admiral-cove-park/, on May 13th, 2022 </small>

### [Fort Needham Park](https://www.halifax.ca/parks-recreation/arts-culture-heritage/halifax-explosion/fort-needham-memorial-park-master-plan)
A newly renovated park in the historic Hydrostone neighborhood of North End Halifax. This park is dedicated to the lives lost in the Halifax Explosion of 1917, and boasts a state-of-the-art playground for children of all ages.

### [Halifax Common](https://www.google.com/maps/place/Halifax+Common/@44.6499143,-63.5893351,17.58z/data=!4m5!3m4!1s0x4b5a222ad81d169d:0x381aec3ed7597adf!8m2!3d44.6499674!4d-63.5889051)

A beautiful urban park located in the center of Halifax. If you're visiting in the winter check out the outdoor skating rink at Emera Oval.


### [Long Lake Provincial Park](https://www.halifaxtrails.ca/long-lake-provincial-park/)

It is a beautiful park located in the outskirts of Halifax. This park s really famous for its long trail where people go on walking usually.<br><br>

### [Point Pleasant Park](https://www.novascotia.com/see-do/attractions/point-pleasant-park/1461)

The park situated in the south end of the Halifax. If you visit there in winter, you can just enjoy a snow world in park. Walking and cycling are both accept in the park.

### [Long Lake Park](https://www.halifaxtrails.ca/long-lake-provincial-park/) 

Really quiet place with beautiful scenery. It's great place to do caoeing. It's located on westren side of halifax on dunbrack street.If you are looking to do stroll,an adventurous hike,paddle,bike ride or swim then this is the place for you.You must visit.

### [DeWolf Park](https://www.tripadvisor.ca/Attraction_Review-g910762-d12817003-Reviews-DeWolf_Park-Bedford_Halifax_Regional_Municipality_Nova_Scotia.html)

Wonderful place for a walk! People walk along the waterfront and enjoy the sites.There is a nice park area if you have children and they usually have fun events in the summer months.

### [Horseshoe Island Park](https://www.yelp.ca/biz/horseshoe-island-park-halifax)
It is on 6939 Upper Grove, Halifax, NS B3H 2M6. It is a small park and has some small benches. You can enjoy the beautiful view of sunrise and sunset here. 

### [Public Gardens](https://www.halifaxpublicgardens.ca/)
This beautiful park is home to a wide variety of plantlife, statues many ducks, and even a tiny scale model of the Titanic floating in a pond. It also has decent Wi-Fi!



 [**Hail Pond Park**](https://www.google.com/search?q=hail%20pond%20park&rlz=1C5CHFA_enCA991CA991&oq=hail+pond+&aqs=chrome.2.69i57j46i512j0i512j0i22i30j0i390l3.10529j0j4&sourceid=chrome&ie=UTF-8&tbs=lf:1,lf_ui:1&tbm=lcl&sxsrf=ALiCzsb1cls3VgOmvYuo2Q9bdX01oGJRKw:1652294742079&rflfq=1&num=10&rldimm=12111580312426070992&lqi=Cg5oYWlsIHBvbmQgcGFya0i8zPHV5YCAgAhaGhACGAAYASIOaGFpbCBwb25kIHBhcmsqAggDkgEEbGFrZaoBDBABKggiBHBhcmsoAA&phdesc=D4kw4nWWsW0&ved=2ahUKEwiI06nJjdj3AhWIlYkEHbU0CEUQvS56BAgPEAE&sa=X&rlst=f#rlfi=hd:;si:12111580312426070992,l,Cg5oYWlsIHBvbmQgcGFya0i8zPHV5YCAgAhaGhACGAAYASIOaGFpbCBwb25kIHBhcmsqAggDkgEEbGFrZaoBDBABKggiBHBhcmsoAA,y,D4kw4nWWsW0;mv:[[44.6335323,-63.632811700000005],[44.6332671,-63.636226199999996]]) Hail Pond park is 3.6 km a way from Dalhousie University a beautiful place for morning walk or to take your dog a walk very quiet place to be a way from traffic.

 [**Oakfield park](https://www.novascotia.com/see-do/outdoor-activities/oakfield-provincial-park/1948)
 Oakfield park is a very popular park located on Shubenacadie Grand which features a small beach and picnic area. 


\### [Shubie Park](https://www.google.com/maps/place/Shubie+Park/@44.701534,-63.5576463,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a26a25bbaf08d:0xd48b34c791afe35!8m2!3d44.7015302!4d-63.5554576)
If you are visiting Dartmouth Crossing, and you want some fresh air, Shubie Park might be the place for you. Not only you can get some fresh air, you get walk along the Shubenacadie Canal, and it cannot get better than this. Also, expect to see some wild animals as well! 
### [Belchers Marsh Park](https://www.halifaxtrails.ca/belchers-marsh/)
It is on Parkland Dr. A beautiful place to take a morning walk or walk dogs. A quite place to do meditations and relax. It gives a feeling of getting out of the city.

### [Westmount School Playground and Splash Pad](https://www.chatterblock.com/resources/55007/westmount-school-splash-pad-halifax-ns/)
Westmount is right across the street from the Halifax Shopping Centre, so easy to access by public transport, or you could combine a trip to the mall with some time on the playground. Address: 6700 Edward Arab Ave, Halifax

### [Peace and Friendship Park](https://fathomstudio.ca/our-work/cornwallis-park)
[**Peace and friendship park**](https://fathomstudio.ca/our-work/cornwallis-park) is located right in front of Westin Hotel in the South End of Halifax. If you are looking for a park to go for jogging, it is a suitable place. 

### [Oakland Road Park](https://goo.gl/maps/k2tNgauFYAE2WusW9)
Oakland Road Park is a small park in Halifax South End. It has a beautiful dock with pretty sunset and lake views, it is a really nice and calm get away from the city while it is about 5 mins of walk from campus.

### [Victoria Park](https://goo.gl/maps/dME8zSf6S9FtQeHT9)
The park is not big but it is a good place to have a rest. Pigeons often gather under the statue, and there are fountains in the summer. It's a great place to take pictures.

### [Frog Pond Park](https://scotiasites.com/frog-pond/)
Frog Pond Park is a small park with a 1.4km trail surrounding a pond. This is a good place to walk around and enjoy the wildlife culminating at the pond, such as ducks, chipmunks, and frogs.

## Chocolatiers

### [Rousseau Chocolatier](https://rousseauchocolatier.ca)
[**Rousseau Chocolatier**](https://rousseauchocolatier.ca) makes one of the best mochas on the city. If you went there, don't forget to order macarons which are quite delicious.

## Beaches

[**Rainbow Haven Beach - Decent beach nearby Halifax**](https://www.novascotia.com/see-do/outdoor-activities/rainbow-haven-beach-provincial-park/2203)

[**Dingle Beach - Sir Sanford Fleming Park**](https://www.novascotia.com/see-do/attractions/sir-sandford-fleming-park-the-dingle/1521) A 95-acre park with four natural habitats (woodlands, heath barren, salt water and pond; please do not feed the waterfowl), walking trails, the Dingle Tower with bronze lions at the foot, a sandy beach (unsupervised swimming), a wharf and a boat launch.

[**Carters Beach**](https://www.sandylanevacations.com/activities.details.php?activity=carters-beach-nova-scotia) Carters Beach is a beautiful beach located in Port Mouton, Nova Scotia only a 2-hour drive from Halifax to the location worth the drive place is trash free water is transparent and crystal blue feels like you are on an island does not feel like Nova Scotia at all. 

[**Clam Harbour Beach Provincial Park**](https://www.novascotia.com/see-do/outdoor-activities/clam-harbour-beach-provincial-park/1717)

Clam Harbour features a long white sand beach with a unique shallow tide stream that often means warmer water for swimmers.\


Lawrencetown Beach Provincial Park, it offers a really long coastline. The weather there could be very windy, check before you go.

[**Kinsmen First Lake Beach - good place to play fireworks**](http://www.sackvillekinsmen.ca/) is located in Kinsmen Park, 71 First Lake Drive. People can play fireworks on New Year's Day or the Spring Festival on that beach with enough light around.

### Black Rock Beach

[**Black Rock Beach**](https://www.halifax.ca/parks-recreation/programs-activities/swimming/supervised-beaches-outdoor-pools-splash-pads) which is at Point Pleasant Park on Sailors Memorial Way. Here is a tourist attraction in Halifax. We can walk around and feel the sea breeze.
[**Google Map Link**](https://goo.gl/maps/LEhaQpPJtXLaZBmM8)


### Chocolate Lake Beach

[**Chocolate Lake Beach**](https://www.google.com/maps/place/Chocolate+Lake/@44.6384459,-63.6259791,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21eccc552311:0x8716c002b46e4423!8m2!3d44.6388728!4d-63.6236068) is an ideal beach resort to drive to with friends or family for a swim on a hot summer day, and it's only a 5-minute drive from the city center.

[**Conrad’s Beach**](https://scotiasites.com/conrads-beach/) a really good place to watch the sunset with your friends. Or to just take a nice walk, the water is a little brisk and usually only gets warm around August.

[**Crystal Crescent Beach**](https://parks.novascotia.ca/park/crystal-crescent-beach) Three white-sand beaches near the mouth of Halifax Harbor. Great place to go on a sunny day.

[**Queensland Beach**](https://www.google.com/maps/place/Queensland+Beach+Nova+Scotia/@44.6351658,-64.0438072,14z/data=!3m1!4b1!4m5!3m4!1s0x4b59ec17e9ca0f8b:0xa54749506702ad77!8m2!3d44.6351677!4d-64.0262976) is a bit further from the heart of the city, but well worth it. Make a trip to Queensland on a hot summer day with family and friends. This beach is supervised, but becomes packed with people very quickly!


[**Williams Lake Beach**](https://www.gov.mb.ca/sd/parks/park-maps-and-locations/western/william.html) 

William Lake is east of the main Turtle Mounta in Provincial Park. It is well-known for its stocked waters which have produced great catches of rainbow trout over the years.


## Restaurants

### [Mezza Lacewood](https://www.mezzalebanesekitchen.com/canada/)
This mezza is the most busy one located in Halifax. It is located at the hotspot of lacewood beside many other food stores. It is also Halal which draws a lot of attention from muslim customers.

### [Spring Garden McDonalds](https://www.mcdonalds.com/ca/en-ca/location/nova-scotia/halifax/spring-garden-rd/5675-spring-garden-road-ste-g08/29712.html)
The McDonalds on Spring Garden road is often quite busy, but it is a great place to go for your 3am McNugget cravings. This is easy to navigate to for Dalhousie students, as it is a straight line east from Coburg Rd.

### [Quinpool Road McDonalds](https://www.mcdonalds.com/ca/en-ca/location/halifax/halifax-quinpool/6324-quinpool-road-/5652.html)
A McDonalds restaurant located on Quinpool road. Contains a drive-thru and a mural of the public gardens. 

[**Wasabi House**](https://wasabihouse.ca/wasabi-house-halifax/)
Their sushi is delicious, and they often give customers extra snacks. Also, I remember they seem to have discounts after 9.00pm.

[**Swiss Chalet**](https://www.swisschalet.com/)is a Canadian chain of casual dining restaurants. The roast pork ribs are delicious.

### [Muir Resturant](https://muirhotel.com/taste/)
[**Muir, Drift, Cafe Lunette**](https://muirhotel.com/taste/) are part of Muir hotel and resturants. If Muir is not the best in Halifax, it is one of the toppest because Muir is one of few resturant mentioned in Forbes, and perfect for formal gatherings and events. Absolutely worths trying.

[**aFrite**](https://afrite.ca/) is a restaurant located at [**1360 Lower Water St**](https://g.page/Afriteresto?share) owned by Masterchef Canada contestant (Seasons 2 and 7) Andrew Al-Khouri. aFrite showcases modern Mediterranean fusion based on Andrew’s mother’s traditional Syrian cooking style. <br/>
![aFrite](images/afrite.jpg) <br/>
<small>Note: This image has been taken from https://afrite.ca/about/ on May 12, 2022 </small>

[**Café Aroma Latino**](https://www.cafearomalatino.com/) is a great spot to try latin american cuisine, there is a variety of dishes from breakfast to lunch that can be enjoyed with region specific drinks. In addition to serving food you are able to purchase some grocery items from these countries.

[**Boondocks**](https://www.boondocksrestaurant.ca/)
Classic Seafood Restaurant on the shore of the inlet, facing McNabs island. The seafood chowder,
and the fish pie are my favourite. Just ask too add salt (They dont put enough).

### Shiraz Restaurant

![Shiraz Restaurant](images/Shiraz.JPG)</br>

<small> Note: This image is take from https://www.tripadvisor.ca/Restaurant_Review-g154976-d790152-Reviews-Shiraz-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html#photos;aggregationId=101&albumid=101&filter=7&ff=75159063 </small>

[**Shiraz**](https://www.tripadvisor.ca/Restaurant_Review-g154976-d790152-Reviews-Shiraz-Halifax_Halifax_Regional_Municipality_Nova_Scotia.html) is famous for its delicious persian cuisine. If you are a Kebab fan then Shiraz should be one of your stops.

**Battery Park Bar and Eatery**\
![Battery Park Bar](images/bar.jpg) <br/>
Note: This image has been taken from their website https://batterypark.ca/ on May 5, 2022

There is a lot of interesting food that could be found in Halifax. Multiple cuisines including Indian, Korean, Japnese, French could be found.
The best place to find cheap and interesting Nachos is Battery Park Beer Bar and Eatery in Dartmouth. The menu and the further details of the bar could be accessed from their website: https://batterypark.ca/
They serve till 11:00 PM, however it is open till 12:00AM. It is the first come and first serve basis and hence no reservations are required.

[**Good Robot**](https://goodrobotbrewing.ca/)
![picture of Good Robot patio, with customers drinking in the sun](images/good-robot-image.jpeg)
<small>image taken from: https://bikefriendlyns.ca/node/111 on May 10th</small>\
Good Robot is a restaurant and bar in the North End of Halifax, started by 3 engineers who were bored of the monotony of their jobs. They came together to create a funky robot themed establishment with a kickin outdoor patio lovingly called the GastroTurf as it is fully astroturf grass.

[**Darrell’s**](https://darrellsrestaurants.com/) restaurant is a great diner type place which serves award-winning burgers and shakes. My go to when going to Darrell's is their award-wining peanut butter burger.

[**Feasts**](https://www.thefeasts.ca/) All dishes are made with delicious fresh ingredients right in front of your eyes! Can be customized as per your preferences:
s
Mappatura - Italian restaurant on Spring Garden Road. Link: https://mappaturabistro.ca/

[**New Panda Buffet**](https://www.google.com/maps/place/New+Panda+Buffet/@44.6802511,-63.5434644,17z/data=!3m2!4b1!5s0x4b5a2409aaa26bb3:0xdc4104961d081df7!4m5!3m4!1s0x4b5a2578f87518ed:0x494ac8e805c9c0a!8m2!3d44.6802473!4d-63.5412757) It's a buffet restaurant with Chinese dishes, Japanese dishes and some Korean dishes. The music in this restaurant is also very unique, all of which are Classical Chinese tunes. This restaurant is very cheap, you can eat all you want for $15.

### Charger Burger 

[**Charger Burger**](https://www.chargerburger.com/) is one of the few Halal burger restaurants in Halifax. They use fresh ingredients from top quality Nova Scotia producers. They have a great dining area with friendly staff.

[**Song's restaurant**](http://www.songskoreanrestaurant.com/) is a small Korean restaurant with a nice atmosphere and good food.

[**JUKAI Japanese & Thai**](https://jukaidartmouth.com/), all you can eat for 29.99 for each person, best sushi i had in HRM

[**Casablanca**](https://casablancahfx.ca/) is an amazing restaurant with great interior that offers delicious and authentic Moroccon food in Halifax.

[**5 Fisherman restuarant**](https://www.fivefishermen.com/) is a place only provide seafood.

[**The Foggy Goggle**](https://www.thefoggygoggle.ca/) The Foggy Goggle serves up a delicious assortment of healthy pub fare classics with a twist. Vegan and gluten-free options available.

[**9+nine**](https://www.9plus9.ca/) is a place to have authentic Chinese food.

[**Bakeaway**](https://www.bakeaway.ca/) Bakeaway is a Middle-Eastern cuisine that contains alot of the most famous foods that are famous
throughout the arab world. It makes in my opinion and other opinions, the best Middle-Eastern dishes in Halifax. It contains dishes such as
Falafel wraps, Shawarma, Vine leaves and many more that are worth the money for.

[**The Keg**](https://kegsteakhouse.com/en/locations/halifax) is a good steak house. They have delicious food, and it is a really prefect place to date. I bet this is the best steak house in Halifax.

[**Sushi Nami Royale**](https://sushinami.ca/) Let's start off by mentioning how its one of the best sushi restaurants in the city by far, everything is fresh, great servers, and great food price isn't to high, but definitely high quality sushi amazing drinks as well.

[**Buta Ramen**](https://www.butaramen.ca) is a good place if you're craving some ramen. They've got other appetizers too like edamame

[**902 restaurant & catering**](https://www.902halifax.com/media) 902 restaurant & catering is Middle Eastern restaurant makes Middle-Eastern Lunch and dinner meals such as Kebabs, Lamb Shank, Roasted chicken, Traditional Dishes, Fried Chicken, Shawarma, Hummus, Wraps, Burgers, it is located at spring garden at 1579 Dresden Row, Halifax, Nova Scotia B3J 2K4

[**Timberlea Tavern**](https://www.facebook.com/thetbr/) is a great place place to enjoy bar foods such as wings, burgers, nachos as well as sandwiches, salads and much more while enjoying a few drinks. Be sure to check their event schedule for live performers.

[**The Board Room Cafe**](https://theboardroomgamecafe.mybigcommerce.com/) is a boardgame cafe located on Barington St. There's a wide variety of games you can play with friends for only $6 per session, plus a menu of snacks, meals, and drinks to keep you going!

[**La Frasca Cibi & Vini**](https://lafrasca.ca) A beautifully designed Italian restaraunt located in the core of Halifax's downtown area at 5650 Spring Garden Rd that combines classic Italian dishes with a modern twist.  A lovely ambience creates a perfect location for a date night or anniversary meal!

[**Boston Pizza**](https://bostonpizza.com/en/locations/downtown-halifax.html) Boston Pizza located on Granville Street has a variety of delicious pizzas and fries, as well as a wide selection of drinks. The clean environment inside the hotel makes you feel comfortable. Customers can customize the pizza they want according to their preferences, 
one of the best pizza places in the Halifax downtown.

[**Sea Smoke Restaurant & Bar**](https://www.seasmokehalifax.com) is an amazing seafood restaurant located in the waterfront with a really beautiful patio with fire pit. They have nice oysters and sushi too.

[**Fan's Chinese Restaurant**](http://www.fansrestaurant.com/) A Chinese restaurant located on Windmill Road in Dartmouth serving dim sum, hotpot, and other dishes. The best dim sum I've had in HRM with lots of selection.

##Adda Indian Eatery

[**Adda Indian Eatery**](https://www.addaindianeatery.com/menus) Located in the Spring Garden Place this restaurant is one of the best Indian restaurants that has the vibes of India and has food to offer from all parts of India and if you feel away from home as an Indian this is a place you shall surely go to and eat food.

###Shadia's Pizza

[**Shadia's Pizza**](https://shadias-pizza.com) the best place to get pizza from, not only have pizza but also burgers and proteins. Highly recommended. Fast delivery and delicious food!. 

[**Murphy's on The Water**](https://www.opentable.com/r/murphys-on-the-water-halifax)a historic location in Halifax that was previously a cable wharf. Nowadays, it is downgraded to a poorly managed restaurant with poor quality, frozen, imported (not local) food. Also, over my summer working there I caught over a dozen rats roaming the kitchen supplies, tableware, and produce. 

###Masala Delight
[**Masala Delight**](https://www.masaladelight.com/) Its a great place for indian cusine especially south indian. They have one of the best dosas in halifax.

### Cha Baa Thai Restaurant
[**Cha Baa Thai Restaurant**](https://chabaathairestaurant.ca)  is a restaurant that serves authentic Thai dishes and is recommended to people who like food with a spicier flavoring to satisfy their palates. Their Red Curry Noodle Soup and Pad Thai is especially delicious.


### Busan Korean BBQ 
[**Busan Korean BBQ**](http://busankoreanbbq.ca/) is a great korean barbeque place to go out with family or friends. It has a lunch special menu at 12:00-4:00pm for affordable and delicious korean dishes.

###Tawa Grill
[**Tawa Grill**](https://176838.com/tawa) makes classic Indian cuisine, great portions and taste for reasonable pricing. They had a day-time lunch buffet for $14, they unfortunately don’t have it anymore. Its ambience makes for a very romantic date spot after evening, and the staff are super nice.

[**360 Lounge**](https://www.360loungehfx.com/) This is a fusion american restaurant which boasts a variety of mouth watering items on its menu. The best part is all the food is halal so it offers options for people of all faiths.
### Wineries

![picture of the Benjamin Bridge patio at sunrise](images/Benjamin-Bridge-at-Sunrise.jpeg) <small>Image taken from https://winesofnovascotia.ca/portfolio/benjamin-bridge/ on May 10th</small>  
[**Benjamin Bridge**](https://benjaminbridge.com/) is an award-winning vineyard located in the heart of Gaspereau Valley, known for it's sparkling whites. You might know it as the brand behind the locally loved Nova 7 wine. They have recently been the buzz of the town as they have introduced the alcohol-free, wine-style beverage Piquet Zero, now being sold at grocery stores near you.

[**L'Acadie Vineyards**](https://www.lacadievineyards.ca/) Located in the beautiful Annapolis Valley. L'Acadie was one of the first the first organic winery in Nova Scotia and has won many awards for their wines in Canada and Internationally. It is definetly worth a stop in for a tasting session if you can make it!

[**Luckett Vineyards**](https://www.luckettvineyards.com/) Located just north of Windsor, overlooking the Gaspereau Valley.  The estate first started growing grapes in 2003, but did not open their doors to the public until 2011, this is a family run vineyard that boasts a lovely restaraunt.  A must stop location for wine tasting in Nova Scotia!


[**Xiaoyu Homestyle Noodle**](https://goo.gl/maps/Mx5zb61Q39ZisTGV7) is an authentic chinese restaurant. You can get some real Chinese style noodles hear. There are some really spicy food.


[**The Wooden Monkey**](https://www.thewoodenmonkey.ca/) 

is really good restaurant esspeccially for seafood. It has two branch one in Halifax and one in Dartmouth. They use locally grown oraganic product.You must visit. 

[**Sushi Jet**](https://goo.gl/maps/VskYQxuoTvEWfHDq8) is a very good Japanese restaurant, very convenient and affordable, I especially like their "Sushi Appetizer".

### Kor-B-Q
[**Kor-B-Q**](https://korbq.ca/)  is a Korean BBQ restaurant located at 278 Lacewood Dr. Halifax, this Korean Grill restaurant serves All-you-can-eat Korean BBQ, the food there taste very good, they have fresh ingredients and premium raw meats, and the price is not very high.
### Chkn Chop

[**Chkn Chop**](https://www.chknchop.com), as implied by the name, is a chicken-oriented restaurant in the north end of Halifax. They serve variety of sandwiches, poutine, cooked chicken, drinks and more. They also have weekly rotations on their sandwiches and wings every Tuesday.

[**HAPPY VEAL HOT POT**](https://happyvealhotpot.com/) This is a very good Chinese restaurant, I often order their food on the takeaway platform, the taste of his food is very good, the taste is also very fragrant, the price is also very good value, is a restaurant that I like very much

[**Sushi Cove**](https://sushicovehalifax.com/) Lovely family run business that has beautifully presented and fresh sushi with original roll combos and the crispiest tempura!
### Jean's Chinese restuarant

[**in spring fine asian fusion cuisine**](http://www.inspringhotpot.com/) If you want to try hot pot or spicy hot pot, you can try this restaurant, he offers great tasting sesame sauce, and the key is that he is buffet!

[**Shanxi Paomo**](https://shanxipaomo.com/) This is a great Chinese restaurant in Quinpool, the Roujiamo (kind of like Chinese burger) and Liangpi (something like cold rice noodles) taste amazing, and the price is not expensive!

[**Lot Six**](http://lotsix.ca/) Yummy place to eat and they have bottomless mimosa brunch on the weekend. It’s also a nice date night spot.

[**Eliot and Vine**](https://eliotandvine.com/) Located on clifton street, Eliot and Vine serves a variety of amazing dishes in a cozy yet fancy enviroment

[**Fries&Co**](https://friesnco.com/menu.html) Located in 2603 Connolly St. It mainly sells the fries and fried fish. It is one of the highest rating fried fish store in Halifax.

[**Woody’s BBQ**](https://www.yellowpages.ca/bus/Nova-Scotia/Dartmouth/Woody-s-Bar-Bq/7933688.html) Their fried corn-on-the-cob with garlic butter is a delicious and unique side. They are open nightly all week. The address is 159 Hector Gate, Dartmouth, NS B3B 0E5.

### Gangnam Korean BBQ Halifax

[**Gangnam Korean BBQ Halifax**](http://gangnamkoreanbbq.ca/) is the best Korean restaurant I think. It's in 1261 Barrington St. it has the best Korean BBQ &amp; cuisine. The ingredients are very fresh and the service is very friendly

### Jean's Chinese restuarant

[**Jean's Chinese restuarant**](https://www.jeansrestaurant.ca/) has many delicious Chinese food, you can choose to order food on the official website or go to the store to taste it at 5972 Spring Garden Rd.

### KFC

[**KFC**](https://www.kfc.ca/menu/overview) it is the only KFC in downtown Halifax if you want the familiar KFC taste in Halifax go to this one.

### Mary's Place Cafe II

[**Mary's cafe**](https://www.facebook.com/marysplacecafe2/) is a very nice diner that is close to Dalhousie university, and thus is a perfect place for students to visit. The food at the Mary's is very tasty and well priced and contains lots of different types of food in each dish. The people working there are very friendly. location on [**map**](https://g.page/marysplacecafe2?share).

[**Armview Restaurant & Lounge**](http://www.thearmview.com/) is an old-school diner that overlooks the arm. They serve breakfast, sandwiches, steaks and burgers.

[**Antojo Tacos & Tequila**](https://antojo.ca/) serves vibrant, Mexican-inspired dishes, plus an impressive collection of tequila and mezcal — all in a stunningly designed, eclectic setting.

[**Friend's Chinese restaurants**](https://www.google.ca/maps/place/Friend's+Chinese+restaurants/@44.6378966,-63.5757484,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a2237c85cfda7:0xce20274e7dcee6e8!8m2!3d44.6379111!4d-63.573569?hl=en) is a Chinese restaurant which is populator in Chinese students. This restaurant also offers takeaway service.

[**Edna**](https://www.ednarestaurant.com/) is a bustling one-room restaurant with a comfy bar. They have a neighbourhood bistro serving locally sourced and seasonally inspired dishes.

### CUT Steakhouse

[**CUT Steakhouse**](https://www.rcr.ca/restaurants/cut-steakhouse/) is one of the most famous steak restaurants in town. Also, this restaurant has the dry aged steak, where you can enjoy the different flavour of steak in Halifax. 

### The Red Asian Fusion Restaurant

[**The Red Asian Fusion Restaurant**](https://goo.gl/maps/A6ACrbmphh6cgqmX7) This Chinese restaurant is my frequent go-to. Every dish of theirs is delicious and large. The Farmhouse's Fried Pork is my most recommended dish.

### Qiu Brothers Dumplings

[**Qiu Brothers Dumplings**](https://qiubrothersdumplings.com/) which is at 1335 Barring Street. Same as the restaurant name, there have some delicious Chinese-style Dumplings. Otherwise, there also have some other Chinese food.
[**Google Map Link**](https://goo.gl/maps/5BbUgWvko2GJfkwn9)

### Halifax Bread Factory
[**Halifax Bread Factory**](https://halifaxbread.ca/) it is the only place in Halifax that bake different variety of persian(Iranian) breads and foods. Very clean and organized place. I am sure you will love this place.
![Halifax Bread Factory](images/bread.jpeg) <br/>
<small> Note: This image has been taken from https://halifax-bread-factory.business.site/, on May 13th, 2022 </small>

### Yonghe Noodle House

[**Yonghe Noodle House**](https://goo.gl/maps/wpP3QUkgGCHoF7uk6) This noodle restaurant is my favorite noodle restaurant, and I like their curry chicken noodles the most.

### Tony's EST.1976

[**Tony's**](https://www.tonysdonair.ca/) is a local donair and pizza shop (family business) in Halifax, opened in 1976. Highly recommend their TONY's FAMOUS DONAIRS. It is almost full of meat inside that not even the pita bread on the outside could wrap it completely (no matter the size).

>>>>>>> HEAD
## Mary's African Cuisine

[**Mary's African Cuisine**](https://www.marysafricancuisine.com) is an authentic African restaurant located on 1701 Barington street. The restaurant offers a variety of foods from all over Africa. For those who are homesick or looking to try some African food, its the perfect place to visit.
=======
### Your Father's Moustache

[**Your Father's Moustache**](http://yourfathersmoustache.ca/) established in 1989, is a family owned pub. The offer live music as well as outdoor seating. The chicken wings are very good!

### Jukai

[**Jukai**](https://jukaidartmouth.com/) I recommad this Jukai all you can eat sushi resturant to you, it is a really good resturant that I always visit. The location is 49 Kings Wharf Place, Dartmouth, NS.

### The Chickenburger

[**The Chickenburger**](https://chickenburger.com/) is a staple of Bedford, a classic dinner/fastfood restraunt located on the Bedford Highway. They've been open since 1940. They sell chicken burgers, ham burgers, hot dogs, fish and chips, and more. I always like getting their milkshakes. 

### Kai Brady's Fancy Dive Bar
[**Kai Brady's Fancy Dive Bar**](http://kaibradys.ca/) This is a very worthwhile meal bar in downtown. They provide rich locally brewed beer and very exquisite food. (Tips: all hamburgers are half price every Wednesday!)

### Montana's Cookhouse
[**Montana's Cookhouse**](https://www.montanas.ca/?gclid=Cj0KCQjwg_iTBhDrARIsAD3Ib5hlkX5J4RYg_7P3v83IHwtk8NasvCZIWuiOf74cmNxa7cogz2qHnugaAi20EALw_wcB) It's a chain restaurant, and their ribs and steaks are delicious. At the same time, the environment and service quality of the store are also very good, let people like it very much.

### Bap House Korean Kitchen

[**Bap House Korean Kitchen**](https://www.baphouse.ca) is a Korean restaurant located on Quinpool Road. It serves with healthy and fresh food. You can also create your own bibimbap!
>>>>>>> 22162a9ca81ca845e36129519e911372207859d4

[**Chef Abod Cafe & Catering**](https://www.chefabod.com/) is a Middle Eastern restaurant that is known for a variety of dishes, which include their Kabsa, Mandi, and Shawarma dishes. They are located on 3217 Kempt Road, and offer Dine-in, Delivery and Pick-Up, as well as catering services.

### [Spring Garden McDonalds]

## Night Clubs

[**The Dome**](https://www.thedome.ca/) Known by many as "Home", the Dome is a staple nightclub of downtown Halifax. A must see for any dance enthusiast or audiophile looking for a thoroughly mediocre experience.

[**The Seahorse Tavern**](https://www.facebook.com/TheSeahorseTavern/) is a hotspot for live music and some of the best parties in the city.

[**Pacifico**](https://www.pacificohalifax.com/) Located on George st in the heart of downtown, Pacifico is famous among locals and vistors for their incredible live Jazz nights on Thursdays.

>>>>>>> HEAD
[**Midtown-Boomers Tavern**](http://www.themidtown.ca) The Midtown Tavern & Lounge is located in downtown Halifax, Nova Scotia directly across from the Dome / Cheers night club. Offering late night beverages and drink specials daily.

[**The Den**](https://www.thedenhfx.ca) The Den is a night club with (Arguabley) one of the best Music in Halifax. With great Athmospher and diverse group of people, it is one of the best places to be on a Friday night.

[**Hide and Seek**](https://www.hideseekhfx.com) Hide and seek is a nightclub located in downtwon Halifax. Not only does it have a clam and chill atmosphere with banging beats, it is also connected to the Loft nightclub making it so that you pay one price for two nightclubs. 

=======
### Midtown-Boomers Tavern
[**Midtown-Boomers Tavern**](http://www.themidtown.ca) The Midtown Tavern & Lounge is located in downtown Halifax, Nova Scotia directly across from the Dome / Cheers night club. Offering late night beverages and drink specials daily. Great atmosphere to drink and dance with great company!
>>>>>>> 22162a9ca81ca845e36129519e911372207859d4
## Universities

### [University of King's College](https://ukings.ca/)
A university located near and working in collaboration with Dalhousie University. Located at 6350 Coburg Rd, Halifax.

[**Mount Saint Vincet University**](https://www.msvu.ca/) A Primary Undergraduate university which locate at 166 Bedford Hwy, Halifax.

[**Dalhousie University**](https://www.dal.ca/) A Medical Doctoral university which locate at 5849 University Ave, downtown Halifax. It has three different campus. It is the third oldest university in Nova Scotia and the fifth oldest university in Canada.

[**Saint Mary's University**](https://www.smu.ca/) A Primary Undergraduate university which locate at 923 Robie St near dalhousie university.

[**Acadia University**](https://www2.acadiau.ca/home.html) A public undergraduate university ranked among the top Canadian institutes is located in Wolfville, N.S.

[**Nova Scotia Community College**](https://www.nscc.ca/) Nova Scotia Community College is located at the end of Barrington St. It contains 130 programs at 14 different campuses in Nova Scotia. NSCC is one of the known Univerisities found in Halifax as it allows their students tobe flexible and offers alot.

**[NSCAD University](https://nscad.ca/)** also called the Nova Scotia College of Art and Design, is a post-secondary art school in Halifax, Nova Scotia, Canada. NSCAD offers an interdisciplinary university experience unlike any other art school in the country.

[**Université Sainte-Anne**](https://www.usainteanne.ca/campus/halifax) Université Sainte-Anne is a french-language university located in 1190 Barrington St.

[**Cape Breton University**](https://www.cbu.ca/) Cape breton University is located in Sydney Cape Breton. It is the only post secondary institution located inside Cape Breton.

[**St. Francis Xavier University**](https://www.stfx.ca/) is an undergraduate university located in Antigonish, Nova Scotia. 
## Tours

[**Harbour Hopper**](https://www.harbourhopper.com/) Located on the iconic waterfront, the 55 minute number #1 boat tour in Halifax operates between the months of May->October.

[**Tall Ship Silva**](https://www.ambassatours.com/experiences) This is a great option to get out on the Halifax harbour and see the city from a different perspective. You can also enjoy a beverage while you take in the sights and are shown the landmarks of Halifax.

## Sports & competition

[**Ski Wentworth**](https://skiwentworth.ca/) This is a great ski hill located about an hour and a half north from Halifax. It is close to the town of Truro and offers many different types of trails and riding conditions.

[**Ontree Park**](https://www.ontreepark.com/) is a place to have outdoor activities in woods.

[**Ski Martock**](https://www.martock.com/) It is my favorite place to go in winter, you can choose ski or snowboard here, the experience is amazing!

### Emera Oval

[**Emera Oval**](https://www.halifax.ca/parks-recreation/programs-activities/outdoor-recreation/emera-oval) is a public outdoor skating surface. We can do in-line skating, cycling, and skateboarding in the summer and ice skating in the winter. 

### Seven Bays Bouldering

[**Seven Bays**](https://sevenbaysbouldering.com) is an indoor bouldering rock climbing gym with an attached cafe and sitting area. Bouldering is the style of rock climbing where you don't attach youself to the wall with a harness, and only climb about 20 feet up. It has a chill vibe and the rock climbing routes range in difficulty from beginner to expert.

[**Halifax Wanderers Stadium/Grounds**](https://hfxwanderersfc.canpl.ca/stadium-profile) Located in the core of downtown Halifax, besides the Halifax Public Gardens, this small stadium is home to the Halifax Wanderers soccer team. 

### East Coast Surf School

[**East Coast Surf School**](https://ecsurfschool.com) is a great way to get into surfing. They give daily surf lessons at Lawrencetown beach and offer rentals if you like to surf but don't have the gear. Surfing is a cool and fun summer activity to try with your friends or family.

### Halifax Junior Bengal Lancers

[**Halifax Junior Bengal Lancers**](http://www.halifaxlancers.com/) is a non-profit horseback riding school with both youth and adult programs. Located in the centre of downtown Halifax, Lancers is a great place to visit to watch horseback riding right in the city.  

## Hikes

[**MacCormacks Beach Provincial Park — a trial near fisherman’s cove**](https://parks.novascotia.ca/park/maccormacks-beach) Is located on 1641 Shore Rd. It is not a long trail,  fitting for short walking and afternoon sunshine enjoying. The Atlantic Ocean is just besides you,  while the seagulls are always flying over your head.

### Duncans Cove

[**Duncans Cove**](https://www.halifaxtrails.ca/duncans-cove/) is a seaside hike along the coastline just south of the Halifax harbour. It connects to two old bunkers as it winds along the rocky shoreline for about 5km

### [Heart Rock / Collpitt Lake Trails](https://www.halifaxtrails.ca/) 
Large Wilderness Park, Volunteer Run. These trails are mainly for Mountain Bikers.

### [Sir Sandford Fleming Park Trail](https://www.halifaxtrails.ca/sir-sandford-fleming-park/)
Sir Sandford Fleming Park Trail is a short hike that consists of a waterfront boardwalk and forested nature trails. The park also connects to the nearby frog pond trail.

### [Chain of Lakes Trail](https://www.halifaxtrails.ca/chain-of-lakes-trail/)

Chain of Lakes Trail is an easy 7.3 km trail passing by multiple lakes in and around Halifax. The trail begins on Joseph Howe Dr, across from the Atlantic Superstore, and ends on Lakeside Park Dr at the entrance of the Beechville Lakeside Timberlea trail.

### Hobsons Lake Trail

[**Hobsons Lake Trail**](https://www.google.com/maps/place/Hobsons+Lake+Trail/@44.683627,-63.7230048,14z/data=!4m13!1m7!3m6!1s0x4b598a67f4d1e3d3:0xf644dde18090792a!2sHobsons+Lake+Trail,+Nova+Scotia+B4B+1S8!3b1!8m2!3d44.6881066!4d-63.7055326!3m4!1s0x4b598a41d92a3f9f:0x87ada53da158abac!8m2!3d44.694922!4d-63.7047474) is about a 20-minute drive from downtown Halifax and is a great outdoor area with lots of rugged trails for adventure lovers to explore.

[**Hemlock Ravine Park**](https://www.novascotia.com/see-do/trails/hemlock-ravine-park/6119) Located on bedford highway, this park contains a unique heart shaped pond along with a variety of trails surrounding it

[**Polly's Cove**](https://www.halifaxtrails.ca/pollys-cove/) a park with 3.9km of marked trails just 2km away from the world famous Peggy's Cove.

[**Long Lake**](https://www.halifaxtrails.ca/long-lake-provincial-park/) a large provincial park with many trails surrounding Long Lake, only a 15 minute drive from downtown!

[**Salt Marsh Trail**](https://www.halifaxtrails.ca/salt-marsh-trail/) A trail that was previously a railway. About 9km long and is quite flat.

### Shaw Wilderness Park
[**Shaw Wilderness Park**](https://www.natureconservancy.ca/en/where-we-work/nova-scotia/featured-projects/Shaw-wilderness-park.html) is a great hiking ground to explore the landscape of Nova Scotia. There is a short 3km trail which allows you to explore nature, and see amazing views. 

### Scotsbay

[**Scotsbay**](https://gitlab.com/daluniversity/explore-halifax.git) If you like hiking, scotsbay is a place that I recommand to you, you can hiking near the beach about 4 hours long. There will be an amazoning view at the end of the hiking trip.

### Admirals Cove

[**Admirals Cove**](https://www.halifaxtrails.ca/admiral-cove-park/) is a pretty easy hike in Bedford that has some pretty stunning views. It's most famous for "Eagles Rock" which is a bolder ontop of a cliff overlooking the Bedford Basin. It's a great place to go and hangout at when no ones else is around.

### Pollets Cove

[**Pollets Cove**](https://www.alltrails.com/trail/canada/nova-scotia/polletts-cove) Located on the cabot trail. Heard many good things about this trail. Great View, and you can also spot wild horses as well. 

## Dessert Shops

### Cora Breakfast and Lunch

[**Cora**](https://www.chezcora.com/en/breakfast-lunch-restaurants/cora-clayton-park-halifax/?utm_source=googlemaps&utm_medium=nova-scotia-halifax-lacewood&utm_campaign=website) may be disguised as a family restaurant but do not be fooled. Cora is a desert shop at its heart. Nearly all the dishes have fruit and deserts at the center with meat used as decorations.

### Dairy Bar

[**Dairy Bar**](https://www.instagram.com/dairybarhfx/?hl=en) is a seasonal ice cream shop on Spring Garden road. It has delicious ice cream, sundaes, and drinks that are worth the wait in their long lines. Dairy Bar is closed in the winter but will be open again in the summer.

### Cacao 70

[**Cacao 70**](https://cacao70.com/en/our-locations/upper-water-st) is located on the Halifax waterfront, and is an ice cream bar and dessert shop. Open year-round, Cacao 70 is a great place to visit with friends. 

### Creamy Rainbow Bakery and Cafe
[**Creamy Rainbow Bakery and Cafe**](https://g.page/creamy-rainbow-bakery-and-cafe?share) A dessert restaurant in Halifax Downtown. Their egg tarts and puffs are very delicate and delicious.

### Le French Fix Pâtisserie
[**Le French Fix Pâtisserie**](https://www.lefrenchfix.com/) is a pastry shop located in Prince St. It's a fine looking and smelling French baking. The pastries and croissants are always fresh. A must try if you haven't visted it yet!

### LOS TOROS Auténtico Español
[**LOS TOROS Auténtico Español**](http://lostoros.ca) is a good spanish restaurant where near the harbour. There are serving the delicious spanish seafood and traditional food. I strongerly recommend the Valencian seafood fried rice. 
## Coffee Shops

[**Golden Bakery**](https://www.goldenbakery.ca/)
Golden bakery is a nice local coffee shop located on bedford highway. 

[**Wired Monk Bistro**](https://www.facebook.com/wiredmonkhalifax) is a small local coffee shop and bistro located in downtown Halifax.

[**Uncommon Grounds**](https://theuncommongroup.com/pages/uncommon-grounds) is a lovely little coffee shop which is a great place to get out of the apartment to study with a warm drink and great food.

[**Coburg Social**](https://coburgsocial.com/) is a great cafe right near Dalhousie's campus. They have a great selection of food and drinks that can be enjoyed inside or on their patio. This cute cafe also sells fun cocktails and other alcohol, and on occasion hosts live music events.

[**Cabin Coffee**](https://cabincoffeehalifax.com/) Cabin Coffee is a one of the oldest family-owned coffee shop located in downtown halifax.

[**Good Luck Cafe**](http://www.manualfoodanddrinkco.com/goodluck) Located at 145 Portland St in downtown Dartmouth, this cute cafe has everything from coffee and pastries to fresh produce and take home meals.

[**Tsujiri**](https://www.tsujiri.ca/) A cafe and desserts shop located in The Curve on South Park Street. They specialize in matcha and sell drinks, sweets, and snacks inspired by traditional Japanese flavours. 

[**The Daily Grind Cafe & Bar**](https://thedailygrindcafebar.ca/) is located near the waterfront and is a great place to study or just catch up with a friend. As the name mentions, it is not only a cafe, but a bar as well which gives you the option to have your favourite beer while enjoying a good book.

[**Coffeeology**](https://99hwany.wixsite.com/coffeeologyespresso/) is a cafe located on Dresden Row. It is a tiny, cozy space known for its laid-back music and friendly environment. The coffee is amazing, and the baked goods are fresh from a local bakery.

[**Narrow Espresso**](https://g.page/narrowespresso?share) If you live near Fenwick street, this is the perfect place for your first cup of coffee in the morning. They open very early and have a rich variety of coffee and very favorable prices.

[**Trident Booksellers & Café**](https://goo.gl/maps/5nAeWcJh9fCTDA9h7) is a really old fashioned cafe with a calm and old style ambiance, though most books might not be relevant but their cafe is interesting as they make their own coffee and have decent variety of items in their bakery.

### Pane ē Circo
[**Pane e Circo**](https://www.panecirco.ca) Pane e Circo is Halifax, Nova Scotia's newest Italian Salumeria, serving up highest quality products to-go, including a vast selection of imported cured meats & cheeses, sourdough breads & artisanal pastries, and even the fresh handmade pastas & sauces you’ve come to love in our Bertossi Group Restaurants! If you get a chance to try, I would get the almond croissant or any of the sandwiches!

### Creamy Rainbow Bakery and Cafe

[**Creamy Rainbow Bakery and Cafe**](https://g.page/creamy-rainbow-bakery-and-cafe?share) is a very good coffee shop, the egg tarts and puffs are very delicious.

### Rabbit Hole Cafe

[**Rabbit Hole Cafe**](https://www.rabbitholecafe.ca/) An elegant Cafe & Dessert Bar located in the heart of downtown Halifax, 1452 Dresden Row. A great rest stop to have a quiet study session, or to have your own personalized cake designed to your liking.

### Dilly Dally Coffee Cafe

[**Dilly Dally Coffee Cafe**](https://my-site-106684-102240.square.site) is a coffee shop located on Quinpool Road. It provides a chill atmosphere with cute decorations. You can grab a coffee or some amazing baked goods here!

[**Glitter Bean Cafe**](https://www.glitterbeancafe.com) is a coffee shop on Spring Garden Road. It often has a seasonal drink on the menu and has a lot of seating for anyone who is looking to study.

[**The Kiwi Cafe**](https://www.thekiwicafe.com)

is in chester which is south shore of nova scotia , beatiful place to have coffee and they have international themes as well. They are located right next to sea which is quite nice if you are beach or sea person. You must visit.

[**LF Bakery**](http://lfbakeryhalifax.com/) is a really yummy bakery where they have a bunch of authentic French pastries and is on Gottingen street in the North end.

### The Bread Lounge Bakery
[**The Bread Lounge**](https://thebreadloungebakery.square.site/) is a bakery and coffee shop located in downtown Halifax on Demone Street. They have pastries, desserts, sandwiches, soup, coffee, and much more. A great place to study, meet with friends, or grab a quick lunch or snack!

## Bubble Tea Shops

[**TSUJIRI**](https://www.instagram.com/tsujiri_ns/?hl=en) is located at 1605 South Park St. It sells mainly Japanese traditional matcha tea and desserts of matcha flavour.

[**Chatime**](http://www.chatimeatlantic.ca/) is located on 1480 Brenton Street. It's a cool shop for getting bubble tea and it has a lot of variety in teas this is what makes it quite good.

[**Zenq**](https://zenqhalifax.com/) is located on 1065 Barrington St. It servers Taiwanese Bubble Tea and Dessert. 

### Hitea

[**Hitea**](https://www.google.com/maps/place/Hi+Tea/@44.6713508,-63.6765064,15z/data=!4m5!3m4!1s0x0:0x32f9fe93fde15b3!8m2!3d44.6713508!4d-63.6765064) Hitea is a really good bubble tea shop and it is located at 480 Parkland Dr, Halifax.

## Tourist spots

[**Victoria Park**](https://en.wikipedia.org/wiki/Victoria_Park,_Halifax,_Nova_Scotia) is a small park that seems like not much at first glance. However, this park is home to a lot of wildlife with a special abundance of pigeons. If you are ever bored and looking for a fun time, visit the Dollarama down the street on spring garden and pick up a bag of bird seed, and you will make many friends in this park

[**Petit Passage Whale Watching**](https://www.ppww.ca) is a place to watch real whale.

[**George's Island**](https://www.pc.gc.ca/en/lhn-nhs/ns/georges/visit) This national park is a quaint little island in the Halifax harbour. Accessible only by boat (or harbour hopper!) this island is rich in history and bio-diversity as it is reportedly [**infested**](https://www.saltwire.com/nova-scotia/news/halifaxs-historic-snake-laden-island-will-be-open-to-the-public-next-summer-340147/) with snakes!

### Halifax Town Clock

[**Halifax Town Clock**](https://www.pc.gc.ca/apps/dfhd/page_fhbro_eng.aspx?id=10277) is one of the most recognizable landmarks in the historic urban core of Halifax. Situated nearby Citadel Hill, tourists love to go to the Town Clock to get a better view of the landscape of downtown Halifax.

## Lighthouse

[**Peggy's Cove Lighthouse**](https://www.novascotia.com/see-do/attractions/peggys-cove-village-and-lighthouse/1468) Peggy's Cove Lighthouse, also known as Peggy's Point Lighthouse, is one of Nova Scotia’s most well-known lighthouses and may be the most photographed in Canada. (from Nova Scotia website)

### Halifax Waterfront

[**Halifax Waterfront**](https://discoverhalifaxns.com/explore/halifax-waterfront/) is a popular attraction spot for tourists. The waterfront contains a boardwalk that stretches several kilometers. This location also houses several popular restaurants and iconic structures like 'The Wave'.

## Buildings

### Halifax City Hall
[**Halifax City Hall**](https://www.halifax.ca/city-hall) This Place constructed between 1887 and 1890 and is the largest and one of the oldest municipal building in Nova Scotia. If you are interested in architecture you should visit this place.
![Halifax City Hall](images/Hall.jpeg) <br/>
<small> Note: This image has been taken from https://www.halifax.ca/city-hall, on May 13th, 2022 </small>

[**The Vuze**](https://www.templetonproperties.ca/apartments-for-rent-halifax/the-vuze) What's special about this building its not something
casual that you would see throughout Nova Scotia. From it's name "The Vuze", Vuze is also spelt as Views which technically from its name you
can tell its like saying the Views. The views is the tallest building in Halifax. Not just Halifax, it's the tallest building in ALL of Nova
Scotia. It contains 28 floors for normal residents, and 5 pent houses. In total its 33 storeys high above the ground. You can find a picture
of The Vuze taken by me while walking in the images folder.

[**The Jade**](https://thejadehalifax.ca/) This is a new building which is located on Barrington St. It is one of the most luxury apartments building in Halifax and has all new facilities. Also, it has a good view of the street or the sea. Here you can enjoy the most comfortable living environment.

[**Atlantic Hotel Halifax**](https://www.atlanticahotelhalifax.com/) This black and white hotel is built on a bustling and lively street. With comfortable rooms and moderate prices, it has become the best choice of many tourists. 
The hotel's excellent location allows people to quickly go to the famous scenic spots in the city.  At the same time, you can overlook the night view of the city at night. Customers can experience comfortable treatment at an inexpensive price.

[**The Paramount**](https://paramount.universalgroup.ca/) It's an apartment in downtowm of Halifax. It has very convenient transportation. As it's in the downtown so if you want to live here it's very convenient to shop.

[**The Peninsula Place**](https://peninsula.universalgroup.ca) The Peninsula Place is located at 1015 Barrington Street, Halifax. It is a nice apartment and very convenient. It is near the Atlantic Superstore and Halifax Railway Station.s

[**Park Victoria**](https://www.capreit.ca/apartments-for-rent/halifax-ns/park-victoria-apartments/) The location of this apartment is excellent, and the price is also very cheap. The apartment is just off Spring Garden Road and is within walking distance of restaurants and various shops.

## Libraries

### Halifax Central Library

[**Halifax Central Library**](https://goo.gl/maps/qKzCZeZMX1jdcRqV8) is a great library. I have been satisfied with each of my visits to the library. the staff and volunteers are very helpful. And using the printing services is always a breeze in the HCL.

### Wallace McCain Learning Commons

[**Wallace McCain Learning Commons**](https://libraries.dal.ca/hours-locations/wmlc.html) is also one of the dalhousie libraries and it is a very quiet place to study and you can also borrow a computer and book a study room.

### KILLAM LIBRARY

[**KILLAM LIBRARY**](https://www.dal.ca/campus-maps/building-directory/studley-campus/killam-library.html) is my favorite place to study, it is in 6225 University Avenue. Killam library has big space and quiet environment. In particular, you can book study rooms.

[**Kellogg Library/Learning Commons**](https://libraries.dal.ca/hours-locations/kllc-cheb.html) Another Dalhousie Library in the CHEB on Carleton campus. It is bright and modern and has lots of study spaces and rooms that can be booked.

### Patrick Power Library
[**Patrick Power Library**](https://goo.gl/maps/s8omy9N22R199qy67) is library of Saint Mary's University. There are sofa in it. It is a peace and comfortable library.

### Keshen Goodman Public Library

[**Keshen Goodman Public Library**](https://www.halifaxpubliclibraries.ca/locations/kg/) lots of amazing books to borrow, many servsee to help newcomers and students. Has a kids section and section to study. This library is one of my personal libraries to study; libraries in downtown Halifax could get loud sometimes and Keshen Goodman Public Library should be a lot quieter. 

### Halifax North Memorial Public Library

[**Halifax North Memorial Public Library**](https://www.halifaxpubliclibraries.ca/locations/n/) it is also another library in comparision to Halifax Central Library. Closer and more accessible for people who live in the north end.

[**Woodlawn Public Library**](https://www.halifaxpubliclibraries.ca/locations/W/) is another library located at 31 Eisener Boulevard in Dartmouth. It is a nice spacious library with many study rooms.

### Bedford Public Library

[**Bedford Public Library**](https://www.halifaxpubliclibraries.ca/locations/BED/) is a library located on Dartmouth located, near the Sunnyside Mall. It's easily accessible through various routes coming from Dartmouth and Bedford. It's a fairly large library with a section for quiet reading along with a section for children to be kept occupied.

## Gyms

### GoodLife Fitness

[**Goodlife Fitness**](https://www.yelp.ca/biz/goodlife-fitness-halifax-12) is one of my favorite places to go. It's in 5657 Spring Garden Rd. it has good fitness equipment, but if you need a coach, it is not cheap.

### Dalplex

[**Dalplex**](https://athletics.dal.ca/facilities/Dalplex.html), situated just across the Studley campus of Dalhousie University, is one of the more popular gyms of central Halifax. Dalplex consists of pools, courts, gym equipment and much more, but is often busy and requires a membership for access.

[**YMCA HEALTH & FITNESS**](https://www.ymca.ca/what-we-offer/health-and-fitness) there is only one location at Halifax for YMCA HEALTH & FITNESS it is mix gym for male and female open every day Wednesday-Friday from 5:45a.m.- 10 p.m. located at 5640 Sackville Street, Halifax NS B3J 1L2 it has 4.5/5 stars a lot of people recommended and they say the staffs are very friendly and nice feel welcoming.

[**House of eights**](https://houseofeights.com/class-schedule/)
a place to dance and relax, make friends with same interests 

### Canada Games Centre

[**Canada Games Centre**](https://canadagamescentre.ca/) is a sports and recreation centre located near Clayton Park in Halifax. The centre was built for the 2011 Canada Games which took place in Halifax. It is now used as a recreational facility, offering a pool with several aquatics programs, a running track, and a gym. One of the most impressive things about the building has to be the water slide which starts inside the building, travels outside for a few loops, before re-entering the building proper
### Fit4Less

[**Fit4Less**](https://www.fit4less.ca/) has multiple locations around the HRM. An affordable alternative to Goodlife starting at just 6.99 every two weeks.

### 30 Minute Hit

[**30 Minute Hit**](https://www.30minutehit.com/) Great gym location, especially for those that enjoy boxing you could get an amazing workout here plus great staffs.

### Impact Fitness
[**Impact Fitness**](https://impactfitness.club/) Small but a great gym, comes with varity of weights and equipments, staff are nice most of the time :)

[**Queensbury Rules Boxing**](https://www.qrulesboxing.com/) is a gym with a variety of classes and an option for sparring for boxers at every level. Trials start at $25 + tax for two weeks and student discount is also offered when purchasing a membership.

[**Titans MMA**](https://duckduckgo.com/?q=titans+mma&atb=v318-1&ia=places&iai=7383383400122142720&iaxm=places) the best MMA gym in Halifax in my opinion. They host a variety of classes for athletes of different levels in MMA, BJJ, boxing, and kickboxing. 

### JBTC - Nova Scotia Junior Badminton Training Center
[**JBTC - Nova Scotia Junior Badminton Training Center**](https://www.nsjbtc.com/) 10,000 sqf of professional facilites dedicated to badminton training and competition, one of the top in Atlantic Canada. The address is 200 Bluewater Rd
Bedford NS, B4B1G9.

## Government

[**City Hall**](https://www.halifax.ca/) a home of government of Halifax, specific tourisms are allowed 

### Service Canada

[**Service Canada**](https://www.servicecanada.info/service-canada-halifax/) is an important place for international students and it is in 1800 Argyle St. If you have visa and tax requirements, you must remember this place.

[**Access Nova Scotia**](https://novascotia.ca/access-locations/) It is probably the only place for you to do the paper work for your motor vehicle. The walk in is now available, you don't have to book an appointment now.

[**Government House**](https://lt.gov.ns.ca/government-house) It is house for governor of Nova Scotia. It is a one of oldest existing government buildings in North America. The house has served for more than 200 years, it is a great Canadian national treasure. 

[**Halifax Regional Police**](https://www.google.com/maps/place/Halifax+Regional+Police/@44.6873787,-63.550852,13z/data=!3m1!5s0x4b5a241182537729:0xb6dd7ed87969bbfb!4m9!1m2!2m1!1spolice+station!3m5!1s0x4b5a241178840083:0x2293522ab90b5882!8m2!3d44.6873787!4d-63.5310199!15sCg5wb2xpY2Ugc3RhdGlvbpIBEXBvbGljZV9kZXBhcnRtZW50) Kind and patient help for people's stranded (lost ID etc) son, in Halifax and hopefully on his way back to Ontario. Thanks NS Regional Police dispatcher.
### City of Halifax 

[**City of Halifax**](https://www.halifax.ca/) If you want to pay for your property taxes or any building permits, you can go to City of Halifax. If you are going to get the street parking permits, you can go to City of Halifax. 

## Asia Supermarkets

[**LOONG 7 MART**](https://loong7mart-asian-grocery-store.business.site/?utm_source=gmb&utm_medium=referral) This is a supermarket that specializes in Chinese and Asian food, with a wide variety of Chinese snacks, such as instant noodles,
snail noodles, hot pot base. As well as some great tasting drinks, if you have the opportunity to come and see

[**Tai Shan mart**](https://goo.gl/maps/5urytQXNeuFR7Jaz7) it's a Chinese supermarket with a lot of Chinese snacks and daily necessities, I really like to buy hot pot base here.

>>>>>>> HEAD
[**M&Y Asian Market**](https://www.google.com/maps/place/M%26Y%E4%B8%AD%E5%9B%BD%E8%B6%85%E5%B8%82%E5%93%88%E6%B3%95Windsor%E5%BA%97Asian+Market/@44.6453015,-63.5989615,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21725b25a5c5:0x1d81a6823adc30a6!8m2!3d44.6454221!4d-63.5968456) This is a Chinese supermarket located at 6238 Quinpool Rd. This supermarket has a wide selection of things. The cashiers in this supermarket are very helpful. The goods in this supermarket are good and cheap.
=======
[**Topfresh Market**](https://goo.gl/maps/UV9Q6knYdMpwATv7A) I think this is the best Chinese supermarket in Halifax. It has a very complete range of Asian food, and the price of meat and vegetables is much cheaper than that in downtown

[**Tian Phat Asian Grocery**](https://www.yelp.ca/biz/tian-phat-asian-grocery-halifax) Our family go to place for buying asian groceries. Staff are great, and it have most the 
ingredient we need. 

[**Taishan Asian Grocery (Downtown)**](https://www.google.ca/maps/place/Taishan+Asian+Grocery+%EF%BC%88Downtown%EF%BC%89/@44.6411204,-63.583754,15z/data=!4m9!1m2!2m1!1sasian+grocery+store!3m5!1s0x4b5a2236a112752d:0x44d6246f8ca412d4!8m2!3d44.641119!4d-63.5750048!15sChNhc2lhbiBncm9jZXJ5IHN0b3JlWhUiE2FzaWFuIGdyb2Nlcnkgc3RvcmWSARNhc2lhbl9ncm9jZXJ5X3N0b3JlmgEkQ2hkRFNVaE5NRzluUzBWSlEwRm5TVVJGTW5GVVQydFJSUkFC?hl=en&authuser=0) This is a grocery that provides various kinds of Asian and Chinese tradition food and cooking supplements. You can go there and feel the same feeling as a Chinese supermarket.

[**M&Y Chinese Grocery**](https://www.google.com/maps/place/M%26Y+%E4%B8%AD%E5%9B%BD%E5%9F%8E%E5%B8%82/@44.6451372,-63.574707,17z/data=!3m2!4b1!5s0x4b5a223358032579:0x7698710c57a11a87!4m5!3m4!1s0x4b5a23dc41256ecf:0x220e0d841dcd8b5f!8m2!3d44.6451334!4d-63.572513) is a modernized convenience store which has many kinds of asia food and drink and this store has 2 chain store. One locate in the downtown and another one is in quinpool. 

[**V&E 's Convenience Plus**](https://ve-convenience-plus.business.site/) is located on 480 Parkland Dr. It's a good Chinese supermarket and most of The dumplings sold in it are handmade, which makes them more delicious than the dumplings sold by others.

>>>>>>> 9d67bb2bfab35c7cafb63392864b09e037d1af6c
### Union Foodmart

[**Union Foodmart**](https://www.google.ca/maps/place/Union+Foodmart+%E8%8F%AF%E8%81%AF/@44.6729347,-63.5851771,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21940bd0ecfb:0x5bf091da6ba992c3!8m2!3d44.6729312!4d-63.5829834?hl=en&authuser=0) is located in Dartmouth. The largest Asia Supermarket that I have known. 

**[South Asian Mart](https://southasianmart.ca/)** offers a complete range of ethnic grocery from India, Pakistan, Bangladesh, Sri Lankan at very competitive prices.

[**Ca-Hoa Grocery**](https://goo.gl/maps/2PKXuKs4TddfP1uZA) is an old Asian shop. You can buy some really local Chinese source and cooking tool here.

[**OCO mart**](https://www.google.com/maps?q=oco+mart&um=1&ie=UTF-8&sa=X&ved=2ahUKEwjPvZ71n9X3AhWRlYkEHd-eAUEQ_AUoAXoECAIQAw) is a korean grocery store where they have variety of korean products.

[**M&Y Asian Grocery**](https://nicelocal.ca/halifax/shops/mywindsorasian_market/reviews/) is located at 6238 Quinpool Rd. It sells a wide range of ingredients, snacks, drinks, and sauce that are made in Asian countries.

[**Food & Nutrition**](https://foodnutrition.ca/contact) It not only focus on selling products, but also gives customers a new view on how to know their body well and how to choose and cook foods.

### Shahi Groceries
[**Shahi Groceries**](https://www.shahigrocery.com/index.php?route=common/home)    is a South Asian grocery store. You can get your choice of groceries and snacks from a selection of goods from your home country.


##Meat Market
[**Baileys Meat Market**](https://baileys-meat-market.business.site/?utm_source=gmb&utm_medium=referral) This is a store where Beef , Chicken and Lamb meat is available along with loads of spices and snacks and food items or drinks from Arab countries and also Pakistan and India.

## Shopping Center

[**Halifax Shopping Centre**](https://halifaxshoppingcentre.com/) The biggest shopping center in Halifax. You can find all kinds of brands there and buy anything you want. Also, there is a Walmart which is opposite to it.

[**Mic Mac Mall**](https://micmacmall.com/) The biggest shopping center in Dartmouth, it offers you the opportunity to shop everything in Dartmouth without crossing the bridge.

[**IKEA Halifax**](https://www.ikea.com/ca/en/stores/halifax/?utm_source=google&utm_medium=organic&utm_campaign=map&utm_content=halifax) the biggest shopping center about furniture in Dartmouth(like shopping center but not exactly), if you want the newest furniture, then you can go there and find if some of them are suitable for you.

[**park lane mall**](http://shopparklane.ca/) a place to go shopping, watching movies, and browsing dollarama with your friends during the weekend

### Scotia Square

[**Scotia Square**](https://scotiasquare.com/) is a shopping centre near the Halifax Waterfront. It is probably best known for its large food court, featuring a variety of different restaurants. Connected to Scotia Square is a bus terminal for Halifax Transit, this makes Scotia Square a hub of sorts for those arriving in Halifax from areas like Dartmouth, giving greater attention to the shops and restaurants featured within. The close proximity to other areas such as the waterfront and Citadel Hill also make this a nice place to visit.

[**Sunnyside Mall**](https://sunnysidemall.ca/) This is one of the 2 malls located in Bedford Novascotia. Its a 30 minute drive from halifax but i wouldnt recommend going here as you can find everything located in this mall also in the HSC.

### Winners

[**Winners**](https://www.winners.ca/en) is a good place for shopping because everything is cheap with high quality. New cloth and designer fashions arrive several times a week.

### Park Lane Mall

[**Park Lane Mall**](http://shopparklane.ca/) is a shopping mall located on Spring Garden Road. With stores like Cleve's and Dollarama, and a Cineplex movie theatre, Park Lane mall is a great place to visit on your next Spring Garden shopping trip. 

### [Dartmouth Crossing](https://www.google.com/maps/place/Dartmouth+Crossing,+Dartmouth,+NS/@44.7013455,-63.576132,15z/data=!3m1!4b1!4m5!3m4!1s0x4b5a26bef921f91d:0xd31163dc6cc44ac8!8m2!3d44.7055743!4d-63.5637721)
Not really a shopping centre it is; however, I am sure you will want to visit at least one of the places here. There are lots of places to get new clothes for the Summer, as well as Costco and Ikea are there too! Plus if you get tired and hungry while you are shopping, there are great food places such as Montana’s or Boston Pizza as well. 




## Live Music
### Durty Nelly's
[**Durty Nelly's**](https://durtynellys.ca/) is a classic irish pub that has live music 7 nights a week! With a great selection of food & drinks it is a must-visit place in Halifax.

### The Old Triangle Irish Alehouse

[**The Old Triangle Irish Alehouse**](https://www.oldtriangle.com/welcome/) The Celtic Heart of the Maritime's! Experience the best of Irish cuisine and drink Halifax has to offer here on 5136 Prince St. Enjoy some of the greatest live music Nova Scotia artists have to offer!

### Split Crow Pub 
[**Split Crow Pub**](https://www.splitcrow.com) The Split Crow Pub is proud to be ‘Nova Scotia’s Original Tavern’ and continues to serve locals and travelers from around the world. A welcoming smile, generous mugs of grog, fantastic food and, of course, toe-tapping music, welcomes you at the Split Crow Pub. Great live music and $2.50 beers during power hour!

### Dalhousie Arts Centre
[**Dalhousie Arts Centre**](https://www.dal.ca/dept/arts-centre.html)
The Dalhousie Arts Centre hosts shows/concerts that are truly breath-taking. I’ve been lucky enough to go to a show by the legendary guitarist Tommy Emmanuel, and a show by the Symphony Nova Scotia playing pieces by my favourite classical composer, Maurice Ravel. They also do other forms of live media.

## Entertainment

![Captured Escape Rooms](images/escape_room.png)

<small>This image has been taken from https://capturedescaperooms.com/rooms/plunder, on May 8, 2022</small>

### Captured Escape Rooms

**[Captured Escape Rooms](https://capturedescaperooms.com/)** are designed for intense participation from the group trying to escape. It’s perfect for team building experience!

### Bára Whitewater Rafting
[**Bára Whitewater Rafting**](https://www.barawhitewater.com/) is the closest rafting location to Halifax for a particularly exciting experience.

### Red Bar

[**Red Bar**](http://www.redbar.ca/) is a karaoke located at 5680 Spring Garden Rd. You can not only show your beautiful voice there, but also enjoy some live music by various bands. 

### Trapped Halifax

[**Trapped Halifax**](https://trapped.com/locations/halifax.html) is a similar establishment located on Barington St, offering a variety of escape rooms for groups to attempt.

### Propeller Arcade

[**Propeller Arcade**](https://drinkpropeller.ca/pages/propeller-arcade) A retro arcade with many pinball machines, ski ball, and other arcade cabinets located in the lower level of Propeller Brewing Company's Gottingen Street taproom. The best pinball selection and my favourite arcade in Halifax. 

### Scotiabank Theatre Halifax

[**Scotiabank Theatre Halifax**](https://www.cineplex.com/Theatre/scotiabank-theatre-halifax) It is only theatre has IMAX in Halifax, which advanced in comfortable chair and good quaillty vision and sounds. 

## Cineplex Park Lane Mall
[**Cineplex Park Lane Mall](https://www.cineplex.com/Theatre/cineplex-cinemas-park-lane) This is a modern multiplex cinema chain screening the latest Hollywood films, plus new independent releases. Located in the Downtown Halifax it is really convenient for everyone to go to and watch movies and have a good time on the weekend. It has movies in both regular and 3D. 
### Yuk Yuk's

[**Yuk Yuk's**](https://www.yukyuks.com/) is a local comedy club and a good place to go for a laugh!

### Nova Tactical

[**Nova Tactical**](https://www.novatactical.ca/) is a good place to experience the joy of shooting. The service includes archery ranges, airsoft ranges and gun ranges, you also can buy firearms and fishing tools.

### Halifax Ghost Walk

[**Halifax Ghost Walk**](https://www.novascotia.com/see-do/tours/halifax-ghost-walk/7926) Experience the haunted history of Halifax by attending the Halifax Ghost Walk tour located at The Old Town Clock on Brunswick Street. Learn about the thrilling events of Halifax's chilling run ins with pirates, ghost tales, and eerie happenings on this journey through the city!


### Get Air Trampoline Park

[**Get Air Trampoline Park**](https://getairsports.com/nova-scotia/) great place for kids and adults, cheap and very interesting. Great place to have activities indoors when the weather isn't sunny.

### The Comedy Cove
[**The Comedy Cove**](https://thecomedy-cove.com/) local stand-up artists trying to make you laugh. Sometimes good, sometimes bad, but fair warning – don’t come if you are easily offended. 


### Mersey Road Paintball

[**Mersey Road Paintball**](https://www.facebook.com/Mersey-Road-Paintball-301437796536617/) is a great oppourtunity to enjoy simulated combat for sport. The field offers walk-on options for new players where they can rent out gear and purchase paintballs on site. Mersey Road contains a mix of wooded areas and civilian style structures.

### The Deck Box

[**The Deck Box**](https://www.thedeckboxhalifax.com/) is a game store for trading card games, board games and video games, including but not limited to Magic the Gathering, Pokemon, Yu-Gi-oh, Dungeons and Dragons, etc. Deck Box also provides a place and space to play card games, as well as air conditioning, which is a good place for trading card game lovers to meet and communicate offline.

### Neptune Theatre

[**Neptune Theatre**](https://www.neptunetheatre.com) is a wonderful theatre located on Argyle Street. Neptune is a great place to watch live performances such as plays, musicals, and concerts.

### Scotiabank Centre

[**The Scotiabank Centre**](https://www.scotiabank-centre.com/) is the largest facility in Atlantic Canada. Home to the hockey team the Halifax Mooseheads and the Halifax Thunderbirds lacrosse team this multi-purpose arena hosts sports games, concerts, and has restaurants. 

### Bowlarama
[**Bowlarama**](https://southcentrebowlarama.ca/en) is a fun place to go to with family and friends. Contains an arcade, bowling alleys, a bar, and also food and drinks so you can easily make the visit a whole day event.

### Playdium
[**Playdium**](https://www.playdium.com/) is a pretty fun place which has a vast assortment of arcade style games, ride ons, VR games and bowling lanes. It also has a in-house restaurant which is pretty decent. A good place to visit over the weekend with family and friends.

### Casino Nova Scotia

[**Casino Nova Scotia**](https://www.google.ca/maps/place/Casino+Nova+Scotia/@44.6527542,-63.5781077,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a23cdfb7fa64f:0x809350b9ce6629ec!8m2!3d44.6526505!4d-63.5760523?hl=en&authuser=0) is the most famous casino in NS province and the staff are very friendly. And they serve a very delicious buffet. The environment in that place is awesome and it is also very close to the sea.

### Parklane Theater
[**Parklane Theater**](https://www.cineplex.com/Theatre/cineplex-cinemas-park-lane)
Parklane theater is located at 5657 Spring Garden Road.  You can watch screenings of the latest Hollywood films, plus new independent releases.

## Market

### Topfresh Market

[**Topfresh Market**](http://topfresh.ca/which) is at 1 Flamingo Dr. You can buy some unique Chinese ingredients, seasonings, and some kitchen utensils and tableware.
[**Google Map Link**](https://goo.gl/maps/inN2ic3BAez25DSCA)

[**Atlantic Superstore**](https://www.atlanticsuperstore.ca) is a Canadian supermarket chain. You can buy almost all kinds of daily necessities and fresh food.

[**Sobeys**](https://www.sobeys.com/stores/sobeys-queen-street/?f=789/)
They sell good salmon cutlets and whole roasted chicken, and it's very close to the apartment Fenwick, which is a great convenience for the residents there

## Retail Stores

### The Black Market Boutique

[**The Black Market Boutique**](https://www.blackmarkethalifax.ca/) is an abundantly colorful store located on Grafton St. featuring crafts and goods from around the world.

### The Loot

[**The Loot**](https://www.instagram.com/theloothfx/?hl=en) is a curated thrift store selling vintage clothing, accessories, and art.

### The Halifax Army Navy Store

[**The Halifax Army Navy Store**](http://www.thehalifaxarmynavystore.net/), (also known as Ron's Army/Navy), is a family owned military surplus store located on Agricola St.

### Strange Adventures: Comics & Curiosities

[**Strange Adventures: Comics & Curiosities**](http://www.strangeadventures.com/) Canada's oddest and award-winning comic book store, located in both downtown Halifax and downtown Dartmouth. Selling comics since 1992.

### Pete's Frootique

[**Pete's Frootique**](https://petes.ca/) is a grocery store with locations at Dresden Row in Halifax, as well as the Sunnyside Mall in Bedford. It has a wide selection of products that are hard to find elsewhere in other, larger grocery stores, including many international products from areas such as Great Britain. It also features a juice bar which serves many different smoothies and juices.

>>>>>>> HEAD
[**Value Village**](https://stores.savers.com/ns/halifax/valuevillage-thrift-store-2046.html) is a thrift store where you can find essentiall everything. From electronics to vintage cloth, you can find anything at Value village.
=======
### Jennifer's of Nova Scotia

[**Jennifer's of Nova Scotia**](https://www.jennifers.ns.ca/) is a locally-owned gift shop. It exclusively carries locally-crafted products meant to share the Atlantic Canadian experience.

###Oldest McDonald's
[**McDonald's Quinpool**](https://www.mcdonalds.com/ca/en-ca/location/halifax/halifax-quinpool/6324-quinpool-road-/5652.html) is one of the busiest stores and is the first ever McDonald's in Halifax which is open 24*7.

### Walmart 

[**Walmart**](https://www.walmart.ca/en) is one of the largest grocery stores in Halifax. You can find all kinds of groceries as well as electronics, home appliances, and stuff like curtains, cushions, and duvets. All at an affordable price. 

### The Monster Comic Lounge

[**Monster Comic Lounge**](https://www.facebook.com/MonsterComicLounge/) is a collectibles store located on Gottingen St. Here, you can purchase and exchange trading cards. Also, you can find a variety of other collectible items including figures, board games and comic books.

### Atlantic Photo Supply

[**Atlantic Photo Supply**](https://www.atlanticphotosupply.com/) is the last retail store in Halifax that still develops colour film on site. If you need to get your camera film developed I reccomend them for fair prices and speedy development times. They also sell camera equipment and prints if your into that sort of thing.
>>>>>>> 22162a9ca81ca845e36129519e911372207859d4

## Events
[**Baisakhi- in Halifax**](https://www.eventbrite.ca/e/baisakhi-in-halifax-tickets-312234320257) was one of the events organised by Utsav at Waterfront, and will be hosted next year as well. For this year we had a lot of fun as we had music, different indian cuisines and a lot of fun loving folks!

### Halifax Burger Bash

[**Halifax Burger Bash**](https://burgerbash.ca/) takes place every year from April 28 to May 7. During that time, Halifax restaurants will sell their creative burgers (more than 130, priced at $7 or more) and donate the proceeds from each burger to [**Feed Nova Scotia**](https://www.feednovascotia.ca/). If you're interested in burgers, don't miss it.

### East Coast Kite Festival

[**East Coast Kite Festival**](https://www.familyfuncanada.com/halifax/east-coast-kite-festival/) is a non-alcoholic, family and kids friendly event. You can come and enjoy your weekend with kites at Citadel Hill. Their team has over 20 new big kites to display including new 3D kites!

### The Busker Festival

[**The Busker Festival**](https://www.buskers.ca) is an annual festival in Halifax showcasing the many diverse talents of busking performers from around the world. The festival ocurs in the summer and is a wonderful way to spend some time on the Halifax Waterfront. 

### Halifax Jazz Festival
[**Halifax Jazz Festival**](http://www.halifaxjazzfestival.ca/)
Designated a Hallmark Event by the Halifax Regional Municipality, the Halifax Jazz Festival attracts up to 65,000 visitors, involves 400 volunteers and employs over 350 local musicians. The Halifax Jazz Festival will be taking place July 13-17, 2022.

## Learning Opportunities
### Halifax Central Library Media Studios
[**The Halifax Central Library Media Studios**](https://www.halifaxpubliclibraries.ca/library-spaces/book-a-space/media-studio/) comes well-equipped with musical instruments, recording equipment, and top-quality software. You can even do video editing, since it has a green-screen in one of the studios. To top it off, you can rent out equipment, and learn various things through your library card which has access to LinkedIn Learning; and ALL of this is FREE!

[**Here We Code**](https://www.herewecode.ca/) movement is about collaboration between industry, school for the evergrowing economic and social future for all communities in Nova Scotia.

## Farmers Markets

[**Bedford basin Farmers Market**](https://www.bedfordbasinmarket.com/) 
Greek owned farmers market, with a bistro, spice market, and imported goods from Europe (Mainly 
Greece and Germany) 

### Halifax Seaport Market
[**Halifax Seaport Market**](https://www.halifaxfarmersmarket.com/)
The Halifax Seaport Farmers' Market was created in 1750 and is the oldest, 
continuously operating farmers’ market in North America.  In July 2022 The 
Halifax Seaport Farmers’ Market will be moving to the west end of Pavilion 23.

### Halifax Color Festival

[**Halifax Color Festival**](https://allevents.in/halifax/halifax-colour-festival-2022/200022309100406) is an annual event where people dance and play with colors. Food trucks are usually available if you want to grab something to eat. It is open to people from all ages. 

## Live In Halifax!
### Welcome to Dalhousie :)
[**Dalhousie University**](https://www.dal.ca/) 
Welcome to Dalhousie University. I hope you can enjoy your university life and arrange your time reasonably. Dalplex and the library are my favorite places.

### My Apartment!!!
[**2600 Beech St**](https://www.google.com/maps/place/The+Jonathan/)
The Jonathan apartment was built just last year. It's also the apartment where I live now. There is a "roof garden" on the top of the apartment building. A lot of people go there for parties

### Atlantic Superstore!!!
[**Atlantic Superstore**](https://www.atlanticsuperstore.ca/) 
During the few years I lived in Halifax, I went to the Superstore every week to buy groceries and goods. I love the atmosphere there and they were very friendly to me.

### Airport
[**Halifax Stanfield International Airport**](https://halifaxstanfield.ca/)
is a biggest airport in the Atlantic coast. There are many airlines to fly all around Canada. Additionally, Halifax Stanfield International Airport also has many international airlines such as the U.S and EU.

### Hardware Store
[**Canada tire**](https://www.canadiantire.ca/en/store-details/ns/halifax-ns-44.html/?utm_source=google&utm_medium=lss&utm_content=44/)
You can find any tools and appliances you want here, they have all kinds of screws and tool kits. When you need to fix somet hing, this will be your first place to go.

##Religion
[**Saint Mary's Cathedral Basilica**](https://www.halifaxyarmouth.org/parishes/parish-directory/item/saint-mary-s-cathedral-basilica-halifax) St. Mary's Cathedral Basilica is the Largest Catholic church in
the Archdiocese which is located in the downtown core of Halifax.

##The supermarket
[**TopFresh**](https://goo.gl/maps/QRsFPyDQgMGapvka7) This place is a super, with all kinds of specialties that can't be found in other supermarkets. The reason why I choose to introduce this supermarket is that it is the largest Chinese supermarket in Halifax.

##The Road
[**Bedford Hwy**](https://goo.gl/maps/nfPsBHohsPsjy6xf9) This road is next to my house and I personally think it is one of the busiest roads in Haffa with traffic jams on the way to school every day. But the sea beside this road is very nice.

##The Toll station
[**A. Murray MacKay Bridge**](https://goo.gl/maps/dctKoaeoP2fNpF1x5) There is a toll booth on the bridge, and each car has to pay $1.25. The reason I'm introducing this toll booth is because I've been there many times and paid a lot of money.

[**Angus L. Macdonald Bridge**](https://www.google.com/maps/place/Angus+L.+Macdonald+Bridge) The MacDonald Bridge is one of 2 bridges which connect Dartmouth and Halifax. It is a toll bridge and each car has to pay $1.25 (cash) or $1.00 (MacPass).

##Railway Station
[**Halifax Railway Station**](https://www.viarail.ca/en/explore-our-destinations/stations/atlantic-canada/halifax) The Halifax Train Station is located at 1174 Hollis street, it is an inter-city terminal in Halifax, it is opened since 1928, and operated by VIA Rail.

###Food
[**Lemon Tree Restaurant**](https://www.google.com/maps?q=lemon+tree+restaurant&rlz=1C1CHBF_enCA887CA887&um=1&ie=UTF-8&sa=X&ved=2ahUKEwi5wPmngt33AhXihIkEHQ7dALgQ_AUoAXoECAIQAw)
Lemon Tree Restaurant is a traditional Turkish cuisine, including vegan & vegetarian options. If you are a BBQ lover do not miss this place. Their signature dish is there lamb BBQ with different side dishes.

###Cinema
[**Cineplex Cinemas Park Lane**](https://www.cineplex.com/Theatre/cineplex-cinemas-park-lane)
Cineplex Cinemas Park Lane is one of the best cinemas in downtown Halifax. It its a bit over average ticket price but you do sit on a soft couch instead of regular hard ones. Great place for movies.

###Beach
[**Black Rock Beach**](https://www.google.com/search?q=halifax%20beach&rlz=1C1CHBF_enCA887CA887&sxsrf=ALiCzsYhjbBcNREo_ETPULiGXjr3z52iuw:1652468878583&ei=iax-Yvxq76im1A-IopTgDg&ved=2ahUKEwiqkIqklt33AhU1pIkEHbv-CNoQvS56BAgCEAE&uact=5&oq=halifax+beach&gs_lcp=Cgdnd3Mtd2l6EAMyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMggIABCABBDJAzIFCAAQgAQyBQgAEIAEMgoIABCABBCHAhAUMgUIABCABDIFCAAQywE6BwgAEEcQsAM6CggAEEcQsAMQyQM6BQgAEJECOgsILhCABBDHARCvAToKCAAQsQMQgwEQCjoLCC4QxwEQrwEQkQJKBAhBGABKBAhGGABQ2AJYuxFgjRVoAXABeACAAYUBiAHOA5IBAzQuMZgBAKABAcgBCMABAQ&sclient=gws-wiz&tbs=lf:1,lf_ui:1&tbm=lcl&rflfq=1&num=10&rldimm=7467107809038930168&lqi=Cg1oYWxpZmF4IGJlYWNoSNr21rXsqoCACFoZEAEYABgBIg1oYWxpZmF4IGJlYWNoKgIIA5IBBHBhcmuqAQ0QASoJIgViZWFjaCgA&phdesc=w76Cs-wZl6c&sa=X&rlst=f#rlfi=hd:;si:7467107809038930168,l,Cg1oYWxpZmF4IGJlYWNoSNr21rXsqoCACFoZEAEYABgBIg1oYWxpZmF4IGJlYWNoKgIIA5IBBHBhcmuqAQ0QASoJIgViZWFjaCgA,y,w76Cs-wZl6c;mv:[[44.7962458,-63.3337192],[44.4425587,-64.09212339999999]];tbs:lrf:!1m4!1u3!2m2!3m1!1e1!1m4!1u2!2m2!2m1!1e1!2m1!1e2!2m1!1e3!3sIAE,lf:1,lf_ui:1)
Black Rock Beach is a famous beach located inside of the Point Pleasant Park. Very beautiful place for early morning summer walk. And also a good place for picnic with friends and family.
